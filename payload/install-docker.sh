#!/usr/bin/env bash


#####
### Exit if docker is already installed
#

M_DOCKERCE=$(dpkg -l | grep -E ^ii\ \ docker-ce\ | awk -F'  ' '{print $2}')
M_DOCKERCLI=$(dpkg -l | grep -E ^ii\ \ docker-ce-cli\ | awk -F'  ' '{print $2}')
M_CONTAINERDIO=$(dpkg -l | grep -E ^ii\ \ containerd.io\ | awk -F'  ' '{print $2}')
M_DOCKER_COMPOSE=$(dpkg -l | grep -E ^ii\ \ docker-compose\ | awk -F'  ' '{print $2}')

# Array to install docker apps if they're missing
DAPPS=()

if [[ "$M_DOCKERCE" == "" ]]; then
    DAPPS+=("docker-ce")
    DAPP_MISSING="true"
fi

if [[ "$M_DOCKERCLI" == "" ]]; then
    DAPPS+=("docker-ce-cli")
    DAPP_MISSING="true"
fi

if [[ "$M_CONTAINERDIO" == "" ]]; then
    DAPPS+=("containerd.io")
    DAPP_MISSING="true"
fi

if [[ "$M_DOCKER_COMPOSE" == "" ]]; then
    DAPPS+=("docker-compose")
    DAPP_MISSING="true"
fi

if [[ ! "$DAPP_MISSING" == "true" ]]; then
    echo "Docker already installed, exiting"
    exit 0
fi
    


#####
### Check if apps missing
#

CA_CERTS=$(dpkg -l | grep -E ^ii\ \ ca-certificates\ | awk -F'  ' '{print $2}')
CURL=$(dpkg -l | grep -E ^ii\ \ curl\ | awk -F'  ' '{print $2}')
GNUPG=$(dpkg -l | grep -E ^ii\ \ gnupg\ | awk -F'  ' '{print $2}')
LSB_RELEASE=$(dpkg -l | grep -E ^ii\ \ lsb-release\ | awk -F'  ' '{print $2}')

# Array to add apps that need to be installed
MAPPS=()

if [[ "$CA_CERTS" == "" ]]; then
    MAPPS+=("ca-certificates")
fi

if [[ "$CURL" == "" ]]; then
    MAPPS+=("curl")
fi

if [[ "$GNUPG" == "" ]]; then
    MAPPS+=("gnupg")
fi

if [[ "$LSB_RELEASE" == "" ]]; then
    MAPPS+=("lsb-release")
fi

#####
### Install missing apps/dependencies
#

if [[ ! "${MAPPS[@]}" == "" ]]; then
    echo "Installing missing apps: ${MAPPS[@]}"
    apt-get -y install "${MAPPS[@]}" || exit 1
fi


#####
### Add docker's GPG key
#

GPGKEY="https://download.docker.com/linux/debian/gpg"
DOCKER_KEY="/usr/share/keyrings/docker-archive-keyring.gpg"

if [[ ! -f "$DOCKER_KEY" ]]; then
    curl -fsSL "$GPGKEY" | gpg --dearmor -o "$DOCKER_KEY" || exit 1
elif [[ -f "$DOCKER_KEY" ]]; then
    echo "Docker gpg key already installed, not reinstalling."
fi

#####
### Set up stable repo
#

echo \
"deb [arch=$(dpkg --print-architecture) signed-by=${DOCKER_KEY}] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null || exit 1

#####
### Install docker
#


if [[ ! "${DAPPS[@]}" == "" ]]; then
    echo "Installing: ${DAPPS[@]}"
    apt-get update && apt-get install "${DAPPS[@]}"
    if [[ ! $? == 0 ]]; then
        echo "Failed to install docker"
        exit 1
    fi
fi
